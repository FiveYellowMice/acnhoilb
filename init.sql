CREATE TABLE "groups" (
  id INTEGER PRIMARY KEY,
  telegram_chat_id INTEGER UNIQUE NOT NULL,
  telegram_pinned_message_id INTEGER,
  telegram_broadcast_enabled BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE islands (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  timezone TEXT NOT NULL,
  hemisphere TEXT NOT NULL CHECK(hemisphere IN ("Northern", "Southern")),
  specialty_fruit TEXT NOT NULL CHECK(specialty_fruit IN ("Cherry", "Orange", "Pear", "Peach", "Apple")),
  resident_services_level INTEGER NOT NULL DEFAULT 0, -- 0: tent, 1: built
  shop_level INTEGER NOT NULL DEFAULT 0, -- 0: none, 1: built, 2: expanded
  tailors_level INTEGER NOT NULL DEFAULT 0, -- 0: none, 1: built
  opened_by INTEGER,
  open_time INTEGER,
  planned_close_time INTEGER,
  privacy TEXT CHECK(privacy IN ("All", "Friends", "Best Friends")),
  dodo_code TEXT CHECK(LENGTH(dodo_code) = 5),
  last_info_update_time INTEGER NOT NULL,
  turnip_sell_price INTEGER,
  turnip_buy_price INTEGER,
  hot_item_1 TEXT,
  hot_item_2 TEXT,
  meteor_shower BOOLEAN NOT NULL DEFAULT FALSE,
  comment TEXT,
  FOREIGN KEY(opened_by) REFERENCES players(id)
);

CREATE TABLE players (
  id INTEGER PRIMARY KEY,
  island INTEGER,
  name TEXT NOT NULL,
  telegram_user_id INTEGER UNIQUE NOT NULL,
  FOREIGN KEY(island) REFERENCES islands(id)
);

CREATE TABLE group_players (
  id INTEGER PRIMARY KEY,
  "group" INTEGER NOT NULL,
  player INTEGER NOT NULL,
  UNIQUE("group", player),
  FOREIGN KEY("group") REFERENCES "groups"(id),
  FOREIGN KEY(player) REFERENCES players(id)
);

CREATE TABLE island_tags (
  id INTEGER PRIMARY KEY,
  island INTEGER NOT NULL,
  tag TEXT NOT NULL,
  FOREIGN KEY(island) REFERENCES islands(id)
);

CREATE TABLE active_npcs (
  id INTEGER PRIMARY KEY,
  island INTEGER NOT NULL,
  name TEXT NOT NULL CHECK(name IN ("Celeste", "Saharah", "C.J.", "Flick")),
  comment TEXT,
  FOREIGN KEY(island) REFERENCES islands(id)
);

CREATE TABLE obtainable_items (
  id INTEGER PRIMARY KEY,
  island INTEGER NOT NULL,
  name TEXT NOT NULL,
  FOREIGN KEY(island) REFERENCES islands(id)
);
