<?php

require_once 'config.php';
require_once 'lib/db.php';
require_once 'lib/telegram.php';
require_once 'lib/shared_task.php';
require_once 'lib/key_times.php';


if (!in_array($_SERVER['REQUEST_METHOD'], ['POST'])) {
  http_response_code(405);
  echo "Unaccepted request method.\n";
  die();
}

if (@$_GET['token'] !== WEBHOOK_TOKEN) {
  http_response_code(403);
  echo "Invalid token.\n";
  die();
}

$update = json_decode(file_get_contents('php://input'), true);

if (!$update) {
  http_response_code(400);
  echo "Improper JSON received.\n";
  die();
}

http_response_code(204);

if (array_key_exists('callback_query', $update)) {
  $callback_query = $update['callback_query'];
  $message = $callback_query['message'] ?? die();
  $message_data = json_decode(base64_decode(array_slice(explode("\n", $message['text'] ?? die()), -1)[0]), true) ?? die();
  $callback_data = $callback_query['data'] ?? null;

  $player_info = run_sql('SELECT id, name, island FROM players WHERE telegram_user_id = :user_id', ['user_id' => $callback_query['from']['id']])[0] ?? null;
  $player_id = $player_info['id'] ?? null;
  $island_id = $player_info['island'] ?? null;

  switch ($message_data['type'] ?? null) {
    case 'open':
      if ($callback_query['from']['id'] != $message_data['telegram_user_id']) {
        send_api_request('answerCallbackQuery', [
          'callback_query_id' => $callback_query['id'],
          'text' => 'You cannot tap on buttons intended for other users.',
          'show_alert' => true,
        ]);
        die();
      }

      $opened_by = run_sql('SELECT id, name FROM players WHERE id = (SELECT opened_by FROM islands WHERE id = :island_id)', ['island_id' => $island_id])[0] ?? null;
      if ($opened_by && $opened_by['id'] != $player_id) {
        send_api_request('answerCallbackQuery', [
          'callback_query_id' => $callback_query['id'],
          'text' => 'Island was already opened by '.$opened_by['name'].'. An island cannot be simultaneously opened by multiple players.',
          'show_alert' => true,
        ]);
        die();
      }

      if ($callback_data == 'Cancel') {
        send_api_request('answerCallbackQuery', ['callback_query_id' => $callback_query['id']]);
        send_api_request('deleteMessage', [
          'chat_id' => $message['chat']['id'],
          'message_id' => $message['message_id'],
        ]);
        die();
      }

      if (!in_array($callback_data, ['All', 'Friends', 'Best Friends'])) {
        send_api_request('answerCallbackQuery', [
          'callback_query_id' => $callback_query['id'],
          'text' => 'Privacy is empty or invalid.',
          'show_alert' => true,
        ]);
        die();
      }

      if ($callback_data == 'All' && !$message_data['dodo_code']) {
        send_api_request('answerCallbackQuery', [
          'callback_query_id' => $callback_query['id'],
          'text' => 'There cannot be an island open to all but has no Dodo Code.',
          'show_alert' => true,
        ]);
        die();
      }

      send_api_request('answerCallbackQuery', [
        'callback_query_id' => $callback_query['id'],
        'text' => 'Opening island...',
      ]);

      run_sql('UPDATE islands SET last_info_update_time = :last_info_update_time, comment = :comment WHERE id = :island_id', ['last_info_update_time' => time(), 'comment' => $message_data['comment'], 'island_id' => $island_id]);
      open_island($island_id, $player_id, null, $callback_data, $message_data['dodo_code']);

      send_api_request('editMessageText', [
        'chat_id' => $message['chat']['id'],
        'message_id' => $message['message_id'],
        'text' => 'Your island is now open.',
      ]);
      break;

    default:
  }
  die();
}

$message = $update['message'] ?? die();
$chat_id = $message['chat']['id'] ?? die();
if (!is_numeric($chat_id)) die();
$text = $message['text'] ?? '';

preg_match('/^\/(\w+)(?:@'.TELEGRAM_USERNAME.')?(?: (.*)|$)/', $text, $matches);
$command = $matches[1] ?? null;
$command_args = trim($matches[2] ?? '');
unset($matches);

$player_info = run_sql('SELECT id, name, island FROM players WHERE telegram_user_id = :user_id', ['user_id' => $message['from']['id']])[0] ?? null;
$player_id = $player_info['id'] ?? null;
$island_id = $player_info['island'] ?? null;

switch ($command) {
  case 'register':
    group_only_operation($message);

    run_sql('INSERT OR IGNORE INTO groups (telegram_chat_id) VALUES (:chat_id)', ['chat_id' => $chat_id]);

    if ($command_args && (mb_strlen($command_args) < 1 || mb_strlen($command_args) > 11)) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Name must be of length 1-11.",
        'disable_web_page_preview' => true,
      ]);
      die();
    }

    if (!$player_id) {
      if ($command_args) {
        run_sql('INSERT INTO players (name, telegram_user_id) VALUES (:name, :user_id)', ['name' => $command_args, 'user_id' => $message['from']['id']]);
      } else {
        send_api_request('sendMessage', [
          'chat_id' => $chat_id,
          'text' => "To register, send this command again with your in-game player name (not the user name in the chat).",
          'disable_web_page_preview' => true,
        ]);
        die();
      }

    } elseif ($command_args) {
      run_sql('UPDATE players SET name = :name WHERE id = :id', ['id' => $player_id, 'name' => $command_args]);
    }
    run_sql('INSERT OR IGNORE INTO group_players ("group", player) SELECT "groups".id, players.id FROM "groups", players WHERE "groups".telegram_chat_id = :chat_id OR players.telegram_user_id = :user_id', ['user_id' => $message['from']['id'], 'chat_id' => $chat_id]);

    $player_info = run_sql('SELECT island, name FROM players WHERE telegram_user_id = :user_id', ['user_id' => $message['from']['id']])[0];

    send_api_request('sendMessage', [
      'chat_id' => $chat_id,
      'text' => "Successfully registered with name <code>".htmlspecialchars($player_info['name'])."</code>. This name should be your in-game player name, rather than the user name in the chat. You can issue the command again if you wish to change your registered name.\n\nIf you wish to use this bot in another group, you'll need to send this command again in that group. You won't need to include your player name in the command again.",
      'disable_web_page_preview' => true,
      'parse_mode' => 'HTML',
    ]);

    if ($player_info['island'] == null) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => 'Now, you can fill out your island information. Tap the button below to proceed.',
        'disable_web_page_preview' => true,
        'reply_markup' => [
          'inline_keyboard' => [
            [
              [
                'text' => 'Create New or Link Existing Island',
                'login_url' => [
                  'url' => WEB_ROOT_URL.'/new-or-link-island.php'
                ],
              ],
            ],
          ],
        ],
      ]);
    }
    break;

  case 'accept_link_island':
    registered_only_operation($message, $island_id);

    $command_args = preg_split('/\s+/', $command_args);
    $accepting_player_id = intval($command_args[0] ?? null);
    $accept_hash = $command_args[1] ?? null;
    $correct_hash = hash_hmac('sha256', 'accept_link_island:'.$accepting_player_id, SERVER_HMAC_SECRET);

    if (!($accepting_player_info = run_sql('SELECT name, island FROM players WHERE id = :player_id', ['player_id' => $accepting_player_id])[0] ?? null) || !hash_equals($correct_hash, $accept_hash)) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Invalid command argument.",
        'disable_web_page_preview' => true,
      ]);
    } elseif ($accepting_player_info['island']) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "The player you are trying to accept already has an island.",
        'disable_web_page_preview' => true,
      ]);
    } else {
      run_sql('UPDATE players SET island = :island_id WHERE id = :player_id', ['island_id' => $island_id, 'player_id' => $accepting_player_id]);
      $island_name = run_sql('SELECT name FROM islands WHERE id = :island_id', ['island_id' => $island_id])[0]['name'];
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Successfully accepted {$accepting_player_info['name']} to link their player data to your island $island_name.",
        'disable_web_page_preview' => true,
      ]);
    }
    break;

  case 'update_island_info':
    registered_only_operation($message, $island_id);

    send_api_request('sendMessage', [
      'chat_id' => $chat_id,
      'text' => "Tap the button below to update your island information.",
      'disable_web_page_preview' => true,
      'reply_markup' => [
        'inline_keyboard' => [
          [
            [
              'text' => 'Update Island Information',
              'login_url' => [
                'url' => WEB_ROOT_URL.'/update-island-info.php'
              ],
            ],
          ],
        ],
      ],
    ]);
    break;

  case 'open':
    registered_only_operation($message, $island_id);

    $command_args = preg_split('/\s+/', $command_args);
    $island_info = run_sql('SELECT timezone, last_info_update_time FROM islands WHERE id = :island_id', ['island_id' => $island_id])[0];

    if (($command_args[0] ?? null) && $island_info['last_info_update_time'] >= key_time('new_day', 'previous', $island_info['timezone'])->getTimestamp()) {
      if (preg_match('/^[0-9A-HJ-NP-Y]{5}$/', $command_args[0])) {
        $dodo_code = $command_args[0];
        $comment = implode(' ', array_slice($command_args, 1));
        if (!$comment) $comment = null;
      } else {
        $dodo_code = null;
        $comment = implode(' ', $command_args);
      }

      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => ($dodo_code ? "Dodo Code: [<code>".$dodo_code."</code>]\n" : '').($comment ? "Comment: ".htmlspecialchars($comment)."\n" : '')."\nAre you sure to open your island like this? Opening the island this way will not allow you to provide any information about your island except a comment. Providing information that were asked on the webpage (e.g. turnip price) within the comment is discouraged. To open your island the usual way, tap the \"Cancel\" button below and send the /open@".TELEGRAM_USERNAME." command again without argument.\n\nIf you are sure to open your island like this, tap one of the other buttons below corresponding to who your island is open to.\n\n<code>".htmlspecialchars(base64_encode(json_encode(['type' => 'open', 'telegram_user_id' => $message['from']['id'], 'dodo_code' => $dodo_code, 'comment' => $comment]))).'</code>',
        'disable_web_page_preview' => true,
        'parse_mode' => 'HTML',
        'reply_markup' => [
          'inline_keyboard' => [
            [
              [
                'text' => 'All',
                'callback_data' => 'All',
              ],
              [
                'text' => 'Friends',
                'callback_data' => 'Friends',
              ],
              [
                'text' => 'Best Friends',
                'callback_data' => 'Best Friends',
              ],
            ],
            [
              [
                'text' => 'Cancel',
                'callback_data' => 'Cancel'
              ],
            ],
          ],
        ],
      ]);

    } else {
      if ($command_args[0] ?? null) {
        send_api_request('sendMessage', [
          'chat_id' => $chat_id,
          'text' => "To open your island with a single command, you need to open your island using the following method at least once today.",
        ]);
      }
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Tap the button below to ensure your island information is up-to-date. Additionally, add some optional information, such as your daily turnip price, to help other players decide whether your island is worth visiting.\n\nThen, tap the \"Save and Open Island\" button on the bottom of the webpage.",
        'disable_web_page_preview' => true,
        'reply_markup' => [
          'inline_keyboard' => [
            [
              [
                'text' => 'Update Island Information and Open Island',
                'login_url' => [
                  'url' => WEB_ROOT_URL.'/update-island-info.php'
                ],
              ],
            ],
          ],
        ],
      ]);
    }
    break;

  case 'close':
    registered_only_operation($message, $island_id);

    $island_info = run_sql('SELECT name, opened_by, open_time FROM islands WHERE id = :island_id', ['island_id' => $island_id])[0];

    if (!$island_info['open_time']) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Your island is already closed.",
        'disable_web_page_preview' => true,
      ]);

    } elseif ($island_info['opened_by'] != $player_id) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Your island is opened by another player, so you cannot close it.",
        'disable_web_page_preview' => true,
      ]);

    } else {
      close_island($island_id);
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "Your island is now closed.",
        'disable_web_page_preview' => true,
      ]);
    }
    break;

  case 'list':
    group_only_operation($message);

    $available_islands = run_sql('
      SELECT players.name as host, islands.name, islands.timezone, islands.planned_close_time, islands.privacy, islands.dodo_code, islands.turnip_sell_price, islands.turnip_buy_price, islands.hot_item_1, islands.hot_item_2, islands.meteor_shower, islands.comment FROM group_players
      INNER JOIN players ON players.id = group_players.player
      INNER JOIN islands ON islands.id = players.island
      WHERE
        group_players."group" =
          (SELECT id FROM "groups"
          WHERE telegram_chat_id = :telegram_chat_id)
        AND islands.opened_by = players.id
        AND islands.open_time;
    ', ['telegram_chat_id' => $chat_id]);

    if ($available_islands) {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "<b>All Open Islands Available to This Group</b>\n\n".
          implode("\n\n", array_map(function($island) {
            return '🏝️ <b>'.htmlspecialchars($island['name'])."</b>\n".
              'Host: '.htmlspecialchars($island['host'])."\n".
              'Open to: '.$island['privacy'].($island['dodo_code'] ? ' with Dodo Code [<code>'.$island['dodo_code'].'</code>]' : '')."\n".
              stringify_island_information($island);
          }, $available_islands)),
        'disable_web_page_preview' => true,
        'parse_mode' => 'HTML',
      ]);
    } else {
      send_api_request('sendMessage', [
        'chat_id' => $chat_id,
        'text' => "There are no open islands available to this group.\n\nIf you want to open your island, send the /open@".TELEGRAM_USERNAME." command.\nIf you have opened your island but it's not shown here, make sure you have registered in this group as well by sending the /register@".TELEGRAM_USERNAME." command.",
        'disable_web_page_preview' => true,
      ]);
    }
    break;

  case 'help':
    send_api_request('sendMessage', [
      'chat_id' => $chat_id,
      'text' => "ACNH Open Island List (<a href=\"https://gitlab.com/FiveYellowMice/acnhoilb\">source code</a>)\nHelps Animal Crossing: New Horizons players to invite others in chat groups.\nMade with turnips by FiveYellowMice.\n\nTo get started, send /register@".TELEGRAM_USERNAME.".\nTo view all open islands available to this group, send /list@".TELEGRAM_USERNAME.".",
      'disable_web_page_preview' => true,
      'parse_mode' => 'HTML',
    ]);
    break;

  case null:
    break;
  default:
    // Unknown command.
}
