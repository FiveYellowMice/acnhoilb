<?php

require_once 'config.php';
require_once 'lib/db.php';
require_once 'lib/webpage.php';


$player_id = verify_player_registered(verify_telegram_login());

$existing_island = run_sql('SELECT id, name, timezone, hemisphere, specialty_fruit, resident_services_level, shop_level, tailors_level, turnip_sell_price, turnip_buy_price, hot_item_1, hot_item_2, meteor_shower, comment FROM islands WHERE id = (SELECT island FROM players WHERE id = :player_id)', ['player_id' => $player_id])[0] ?? null;
$filled_form = $existing_island ?? [];

$error_msg = null;
$success = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  // Trim every input
  foreach (['name', 'timezone', 'hemisphere', 'specialty_fruit', 'resident_services_level', 'shop_level', 'tailors_level', 'turnip_sell_price', 'turnip_buy_price', 'hot_item_1', 'hot_item_2', 'meteor_shower', 'comment'] as $field_name) {
    $filled_form[$field_name] = trim($_POST[$field_name] ?? '');
  }
  // Convert every integer input to integer type
  foreach (['resident_services_level', 'shop_level', 'tailors_level', 'turnip_sell_price', 'turnip_buy_price'] as $field_name) {
    if ($filled_form[$field_name] === '') {
      $filled_form[$field_name] = null;
    } else {
      $filled_form[$field_name] = intval($filled_form[$field_name]);
    }
  }
  $filled_form['meteor_shower'] = boolval($filled_form['meteor_shower']);
  // Make optional string fields null if they are empty
  foreach (['hot_item_1', 'hot_item_2', 'comment'] as $field_name) {
    $filled_form[$field_name] = $filled_form[$field_name] ?: null;
  }

  // Validate each input
  if (!$filled_form['name'] || mb_strlen($filled_form['name']) > 11) {
    $error_msg = 'Island Name must be of length 1-11.';
  } elseif (!in_array($filled_form['timezone'], DateTimeZone::listIdentifiers())) {
    $error_msg = 'Timezone not chosen or is invalid.';
  } elseif (!in_array($filled_form['hemisphere'], ['Northern', 'Southern'])) {
    $error_msg = 'Hemisphere not chosen or is invalid.';
  } elseif (!in_array($filled_form['specialty_fruit'], ['Cherry', 'Orange', 'Pear', 'Peach', 'Apple'])) {
    $error_msg = 'Specialty Fruit not chosen or is invalid.';
  } elseif ($filled_form['resident_services_level'] === null || $filled_form['resident_services_level'] < 0 || $filled_form['resident_services_level'] > 1) {
    $error_msg = 'Resident Services Level not chosen or is invalid.';
  } elseif ($filled_form['shop_level'] === null || $filled_form['shop_level'] < 0 || $filled_form['shop_level'] > 2) {
    $error_msg = 'Shop Level not chosen or is invalid.';
  } elseif ($filled_form['tailors_level'] === null || $filled_form['tailors_level'] < 0 || $filled_form['tailors_level'] > 1) {
    $error_msg = 'Tailors Level not chosen or is invalid.';
  } elseif ($filled_form['turnip_sell_price'] < 0 || $filled_form['turnip_sell_price'] > 700) {
    $error_msg = 'Turnip Sell Price must be a value between 0-700.';
  } elseif ($filled_form['turnip_buy_price'] < 0 || $filled_form['turnip_buy_price'] > 200) {
    $error_msg = 'Turnip Buy Price must be a value between 0-200.';
  } else {
    // Success
    $filled_form['last_info_update_time'] = time();
    if ($existing_island) {
      run_sql('UPDATE islands SET name = :name, timezone = :timezone, hemisphere = :hemisphere, specialty_fruit = :specialty_fruit, resident_services_level = :resident_services_level, shop_level = :shop_level, tailors_level = :tailors_level, last_info_update_time = :last_info_update_time, turnip_sell_price = :turnip_sell_price, turnip_buy_price = :turnip_buy_price, hot_item_1 = :hot_item_1, hot_item_2 = :hot_item_2, meteor_shower = :meteor_shower, comment = :comment WHERE id = :id', $filled_form);
    } else {
      $db->beginTransaction();
      run_sql('INSERT INTO islands (name, timezone, hemisphere, specialty_fruit, resident_services_level, shop_level, tailors_level, last_info_update_time) VALUES (:name, :timezone, :hemisphere, :specialty_fruit, :resident_services_level, :shop_level, :tailors_level, :last_info_update_time)', $filled_form);
      $island_id = $db->lastInsertId();
      run_sql('UPDATE players SET island = :island_id WHERE id = :player_id', ['island_id' => $island_id, 'player_id' => $player_id]);
      $db->commit();
    }
    $success = true;
    if ($_POST['redir_open_island'] ?? null) {
      http_response_code(303);
        header('Location: '.WEB_ROOT_URL.'/open-island.php?'.http_build_query($telegram_login_parameters));
      die();
    }
  }

} elseif (in_array($_SERVER['REQUEST_METHOD'], ['GET', 'HEAD'])) {

} else {
  http_response_code(405);
  echo "Unaccepted request method.\n";
  die();
}

if ($error_msg) {
  http_response_code(400);
}
webpage_head('Update Island Information');
?>
<?php if ($error_msg) { ?>
<div class="alert alert-danger" role="alert">
  <?= $error_msg ?>
</div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success" role="alert">
  <?php if ($existing_island) { ?>
  Saved.
  <?php } else { ?>
  Your island information has been saved! You can now close this page.<br>
  To open your island, send the <code>/open@<?= TELEGRAM_USERNAME ?></code> command in chat.<br>
  If you wish to update your island information in the future, send the <code>/update_island_info@<?= TELEGRAM_USERNAME ?></code> command to visit this webpage again.
  <?php } ?>
</div>
<?php } ?>
<?php if ($existing_island) { ?>
<p id="text-island-name">Island name: <?= htmlspecialchars($existing_island['name']) ?></p>
<p id="hidden-fields-notice">
  Some fields have been hidden because they usually shouldn't be changed. However, if you want to change them anyway, <a id="show-hidden-fields-button" aria-role="button" href="javascript:void();">tap here</a>.
</p>
<style>
  .form-island-permanent-info { display: none; }
</style>
<noscript><style>
  .form-island-permanent-info { display: block; }
  #text-island-name, #hidden-fields-notice { display: none; }
</style></noscript>
<script>document.addEventListener('DOMContentLoaded', function() {
  $('#show-hidden-fields-button').click(function(event) {
    event.preventDefault();
    $('.form-island-permanent-info').show();
    $('#text-island-name, #hidden-fields-notice').hide();
  });
});</script>
<?php } ?>
<form method="post">
  <div class="form-group form-island-permanent-info">
    <label for="form-island-name">Island Name (required)</label>
    <input type="text" class="form-control" id="form-island-name" name="name" aria-describedby="form-island-name-help" value="<?= htmlspecialchars($filled_form['name'] ?? $_GET['island_name'] ?? null) ?>">
    <small id="form-island-name-help" class="form-text">The island name created on the first day in game.</small>
  </div>
  <div class="form-group form-island-permanent-info">
    <label for="form-island-timezone">Timezone (required)</label>
    <select class="form-control" id="form-island-timezone" name="timezone" aria-describedby="form-island-timezone-help">
      <option disabled <?= ($filled_form['timezone'] ?? null) ? '': 'selected' ?> value="">-</option>
      <?php foreach (DateTimeZone::listIdentifiers() as $tz) { ?>
      <option <?= ($filled_form['timezone'] ?? null) == $tz ? 'selected' : '' ?>><?= htmlspecialchars($tz) ?></option>
      <?php } ?>
    </select>
    <small id="form-island-timezone-help">The timezone set on your Nintendo Switch. This will be used for determining various availability on your island (e.g. shop open).</small>
  </div>
  <div class="form-group form-island-permanent-info">
    <label for="form-island-hemisphere">Hemisphere (required)</label>
    <select class="form-control" id="form-island-hemisphere" name="hemisphere">
      <option disabled <?= ($filled_form['hemisphere'] ?? null) ? '': 'selected' ?> value="">-</option>
      <?php foreach(['Northern', 'Southern'] as $v) { ?>
      <option <?= ($filled_form['hemisphere'] ?? null) == $v ? 'selected' : '' ?>><?= $v ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group form-island-permanent-info">
    <label for="form-island-specialty-fruit">Specialty Fruit (required)</label>
    <select class="form-control" id="form-island-specialty-fruit" name="specialty_fruit">
      <option disabled <?= ($filled_form['specialty_fruit'] ?? null) ? '': 'selected' ?> value="">-</option>
      <?php foreach(['Cherry', 'Orange', 'Pear', 'Peach', 'Apple'] as $v) { ?>
      <option <?= ($filled_form['specialty_fruit'] ?? null) == $v ? 'selected' : '' ?>><?= $v ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group">
    <label>What does your resident services look like? (required)</label>
    <div>
      <?php foreach (['A tent', 'A building'] as $i => $v) { ?>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="resident_services_level" value="<?= $i ?>" <?= ($filled_form['resident_services_level'] ?? null) !== null && $filled_form['resident_services_level'] == $i ? 'checked' : '' ?> id="form-island-resident-services-level-<?= $i ?>">
        <label class="form-check-label" for="form-island-resident-services-level-<?= $i ?>"><?= $v ?></label>
      </div>
      <?php } ?>
    </div>
  </div>
  <div class="form-group">
    <label>What is your shop (Nook's Cranny) like? (required)</label>
    <div>
      <?php foreach (["I don't have a dedicated shop yet", 'It has been built', 'It has been upgraded'] as $i => $v) { ?>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="shop_level" value="<?= $i ?>" <?= ($filled_form['shop_level'] ?? null) !== null && $filled_form['shop_level'] == $i ? 'checked' : '' ?> id="form-island-shop-level-<?= $i ?>">
        <label class="form-check-label" for="form-island-shop-level-<?= $i ?>"><?= $v ?></label>
      </div>
      <?php } ?>
    </div>
  </div>
  <div class="form-group">
    <label>What is your tailor shop (Able Sisters) like? (required)</label>
    <div>
      <?php foreach (["I don't have a tailor shop yet", 'It has been built'] as $i => $v) { ?>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="tailors_level" value="<?= $i ?>" <?= ($filled_form['tailors_level'] ?? null) !== null && $filled_form['tailors_level'] == $i ? 'checked' : '' ?> id="form-island-tailors-level-<?= $i ?>">
        <label class="form-check-label" for="form-island-tailors-level-<?= $i ?>"><?= $v ?></label>
      </div>
      <?php } ?>
    </div>
  </div>
  <?php if ($existing_island) { ?>
  <hr>
  <p>The following information are temporary in game, and will be automatically cleared when they are no longer relevant (e.g. when a day has passed since the last opening). They are not required, but will help other players decide if your island is worth visiting should you open it.</p>
  <div class="row">
    <div class="col-sm form-group">
      <label for="form-island-turnip-sell-price">Turnip Sell Price (optional)</label>
      <input type="number" class="form-control" id="form-island-turnip-sell-price" name="turnip_sell_price" aria-describedby="form-island-turnip-sell-price-help" value="<?= $filled_form['turnip_sell_price'] ?? '' ?>">
      <small id="form-island-turnip-sell-price-help" class="form-text">The price which a player can sell turnips at Nook's Cranny. Only relevant on Mondays-Saturdays</small>
    </div>
    <div class="col-sm form-group">
      <label for="form-island-turnip-buy-price">Turnip Buy Price (optional)</label>
      <input type="number" class="form-control" id="form-island-turnip-buy-price" name="turnip_buy_price" aria-describedby="form-island-turnip-buy-price-help" value="<?= $filled_form['turnip_buy_price'] ?? '' ?>">
      <small id="form-island-turnip-buy-price-help" class="form-text">The price which a player can buy turnips from Daisy Mae on Sundays. Only relevant on Sundays.</small>
    </div>
  </div>
  <div class="row">
    <div class="col-sm form-group">
      <label for="form-island-hot-item-1">Hot Item 1 (optional)</label>
      <input type="text" class="form-control" id="form-island-hot-item-1" name="hot_item_1" aria-describedby="form-island-hot-item-1" value="<?= htmlspecialchars($filled_form['hot_item_1'] ?? '') ?>">
      <small id="form-island-hot-item-1" class="form-text">The item which can be sold for double the price at Nook's Cranny.</small>
    </div>
    <div class="col-sm form-group">
      <label for="form-island-hot-item-2">Hot Item 2 (optional)</label>
      <input type="text" class="form-control" id="form-island-hot-item-2" name="hot_item_2" aria-describedby="form-island-hot-item-2" value="<?= htmlspecialchars($filled_form['hot_item_2'] ?? '') ?>">
      <small id="form-island-hot-item-2" class="form-text">The second item which can be sold for double the price at Nook's Cranny. Only relevant if you have upgraded your shop.</small>
    </div>
  </div>
  <div class="form-group">
    <label>Meteor Shower (optional)</label>
    <div aria-describedby="form-island-meteor-shower-help">
      <?php foreach (['No', 'Yes'] as $i => $v) { ?>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="meteor_shower" value="<?= $i ?>" <?= $filled_form['meteor_shower'] == $i ? 'checked' : '' ?> id="form-island-meteor-shower-<?= $i ?>">
        <label class="form-check-label" for="form-island-meteor-shower-<?= $i ?>"><?= $v ?></label>
      </div>
      <?php } ?>
    </div>
    <small id="form-island-meteor-shower-help" class="form-text">Whether your island has meteor shower tonight. When it does, the daily announcement will mention it, and some animals may also suggest it during daytime.</small>
  </div>
  <div class="form-group">
    <label for="form-island-comment">Comment (optional)</label>
    <textarea class="form-control" id="form-island-comment" name="comment" aria-describedby="form-island-comment"><?= htmlspecialchars($filled_form['comment'] ?? '') ?></textarea>
    <small id="form-island-comment" class="form-text">Any additional information you want to tell other players.</small>
  </div>
  <?php } ?>
  <button type="submit" class="btn btn-success">Save</button>
  <?php if ($existing_island) { ?>
  <input type="submit" class="btn btn-primary" name="redir_open_island" value="Save and Open Island >>">
  <?php } ?>
</form>
<?php
webpage_tail();
