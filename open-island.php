<?php

require_once 'config.php';
require_once 'lib/db.php';
require_once 'lib/webpage.php';
require_once 'lib/telegram.php';
require_once 'lib/key_times.php';
require_once 'lib/shared_task.php';


$player_id = verify_player_registered(verify_telegram_login());

$island = run_sql('SELECT id, name, timezone, opened_by, open_time, planned_close_time, privacy, dodo_code FROM islands WHERE id = (SELECT island FROM players WHERE id = :player_id)', ['player_id' => $player_id])[0] ?? null;

if (!$island) {
  http_response_code(404);
  echo "You do not have an island linked with your player data yet, please create or link one by sending the <code>/register@".TELEGRAM_USERNAME."</code> command.\n";
  die();
}

$filled_form = $island;
if ($filled_form['planned_close_time']) {
  $planned_close_time = new DateTime('@'.$filled_form['planned_close_time']);
  $planned_close_time->setTimezone(new DateTimeZone($island['timezone']));
  $filled_form['planned_close_time'] = $planned_close_time->format('H:i');
} else {
  $planned_close_time = null;
}

$error_msg = null;
$success = false;

if ($island['opened_by'] && $island['opened_by'] != $player_id) {
  $opened_player_name = run_sql('SELECT name FROM players WHERE id = :player_id', ['player_id' => $island['opened_by']])[0]['name'];
  $error_msg = 'Island was already opened by '.htmlspecialchars($opened_player_name).'. An island cannot be simultaneously opened by multiple players.';

} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
  // Trim every input
  foreach (['planned_close_time', 'privacy', 'dodo_code'] as $field_name) {
    $filled_form[$field_name] = trim($_POST[$field_name] ?? '');
  }
  // Make Dodo Code all upper case
  $filled_form['dodo_code'] = strtoupper($filled_form['dodo_code']);
  // Make optional string fields null if they are empty
  foreach (['planned_close_time', 'dodo_code'] as $field_name) {
    $filled_form[$field_name] = $filled_form[$field_name] ?: null;
  }

  do {
    if ($filled_form['planned_close_time']) {
      $planned_close_time = DateTime::createFromFormat('H:i', $filled_form['planned_close_time'], new DateTimeZone($island['timezone']));
      if (!$planned_close_time) {
        $error_msg = "Planned Close Time must be in the format of HH:MM";
        break;
      }
      if (intval($planned_close_time->format('G')) < 5 || intval($planned_close_time->format('G')) == 5 && intval((new DateTime('now'))->format('G')) > 5) {
        $planned_close_time->add(new DateInterval('P1D'));
      }
      if ($planned_close_time <= new DateTime('now')) {
        $error_msg = "Planned Close Time cannot be earlier than now.";
        break;
      }
      if ($planned_close_time > key_time('new_day', 'next', $island['timezone'])) {
        $error_msg = "Planned Close Time cannot be later than the next 5:00.";
        break;
      }
    } else {
      $planned_close_time = null;
    }
    if (!in_array($filled_form['privacy'], ['All', 'Friends', 'Best Friends'])) {
      $error_msg = "Privacy is empty or invalid.";
      break;
    }
    if ($filled_form['dodo_code'] && !preg_match('/^[0-9A-HJ-NP-Y]{5}$/', $filled_form['dodo_code'])) {
      $error_msg = "Dodo Code is invalid.";
      break;
    }
    if ($filled_form['privacy'] == 'All' && !$filled_form['dodo_code']) {
      $error_msg = "There cannot be an island open to all but has no Dodo Code.";
      break;
    }

    open_island($filled_form['id'], $player_id, $planned_close_time, $filled_form['privacy'], $filled_form['dodo_code']);
    $success = true;
  } while (0);

} elseif (in_array($_SERVER['REQUEST_METHOD'], ['GET', 'HEAD'])) {

} else {
  http_response_code(405);
  echo "Unaccepted request method.\n";
  die();
}

if ($error_msg) {
  http_response_code(400);
}
webpage_head('Open Island');
?>
<?php if ($error_msg) { ?>
<div class="alert alert-danger" role="alert">
  <?= $error_msg ?>
</div>
<?php } ?>
<?php if ($success || $island['open_time']) { ?>
<div class="alert alert-success" role="alert">
  Your island is now open. All members in the groups you have registered in can see your island information in their Open Island Lists. If it's only shown in some groups but not the others, make sure you have registered in those groups by sending the <code>/register@<?= TELEGRAM_USERNAME ?></code> command. When you want to close your island, send the <code>/close@<?= TELEGRAM_USERNAME ?></code> command.
</div>
<?php } ?>
<p>Island name: <?= htmlspecialchars($island['name']) ?></p>
<form method="post">
  <div class="form-group">
    <label for="form-island-planned-close-time">When do you plan to close your island? (optional)</label>
    <div class="input-group date" id="form-island-planned-close-time-input-group" data-target-input="nearest">
      <input type="time" class="form-control datetimepicker-input" id="form-island-planned-close-time" name="planned_close_time" data-target="#form-island-planned-close-time-input-group" value="<?= $filled_form['planned_close_time'] ?>">
      <div class="input-group-append" data-target="#form-island-planned-close-time-input-group" data-toggle="datetimepicker">
        <div class="input-group-text">Pick Time</div>
      </div>
    </div>
    <script>document.addEventListener('DOMContentLoaded', function() {
      $('#form-island-planned-close-time-input-group').datetimepicker({format: 'HH:mm'});
    });</script>
  </div>
  <div class="form-group">
    <label>Who is your island open to? (required)</label>
    <div>
      <?php foreach (['All', 'Friends', 'Best Friends'] as $i => $v) { ?>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="privacy" value="<?= $v ?>" <?= $filled_form['privacy'] == $v ? 'checked' : '' ?> id="form-island-privacy-<?= $i ?>">
        <label class="form-check-label" for="form-island-privacy-<?= $i ?>"><?= $v ?></label>
      </div>
      <?php } ?>
    </div>
  </div>
  <div class="form-group">
    <label for="form-island-dodo-code">If your island requires a Dodo Code to be accessed, what is it? (optional)</label>
    <input type="text" class="form-control" id="form-island-dodo-code" name="dodo_code" aria-describedby="form-island-dodo-code-help" maxlength="5" value="<?= $filled_form['dodo_code'] ?>">
    <small id="form-island-dodo-code-help" class="form-text">If you don't provide a Dodo Code, your island will be displayed as it doesn't need a Dodo Code.</small>
  </div>
  <a class="btn btn-success" href="<?= htmlspecialchars(WEB_ROOT_URL.'/update-island-info.php?'.http_build_query($telegram_login_parameters)) ?>">&lt;&lt; Update Island Information</a>
  <button type="submit" class="btn btn-primary">Open Island</button>
</form>
<?php
webpage_tail();
