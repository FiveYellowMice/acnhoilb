<?php

require_once 'config.php';
require_once 'lib/db.php';
require_once 'lib/webpage.php';


$player_id = verify_player_registered(verify_telegram_login());

// Redirect players who already has an island
if (run_sql('SELECT island FROM players WHERE id = :id', ['id' => $player_id])[0]['island']) {
  http_response_code(303);
  header('Location: '.WEB_ROOT_URL.'/update-island-info.php?'.http_build_query($telegram_login_parameters));
  die();
}

$error_msg = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $island_name = trim($_POST['island_name'] ?? '');

  if (!$island_name) {
    $error_msg = 'Island Name cannot be empty.';
  } elseif ($search_results = run_sql('SELECT id, name FROM islands WHERE INSTR(LOWER(name), LOWER(:search)) OR INSTR(LOWER(:search), LOWER(name))', ['search' => $island_name])) {
    foreach ($search_results as &$search_result) {
      $search_result['members'] = array_map(function($i) { return $i['name']; }, run_sql('SELECT name FROM players WHERE island = :island_id', ['island_id' => $search_result['id']]));
    }
  } else {
    http_response_code(303);
    header('Location: '.WEB_ROOT_URL.'/update-island-info.php?'.http_build_query(array_merge([
      'island_name' => $_POST['island_name'],
    ], $telegram_login_parameters)));
    die();
  }

} elseif (in_array($_SERVER['REQUEST_METHOD'], ['GET', 'HEAD'])) {
  $search_results = null;

} else {
  http_response_code(405);
  echo "Unaccepted request method.\n";
  die();
}

if ($error_msg) {
  http_response_code(400);
}
webpage_head('Create New or Link Existing Island');
?>
<?php if ($error_msg) { ?>
<div class="alert alert-danger" role="alert">
  <?= $error_msg ?>
</div>
<?php } ?>
<?php if ($search_results) { ?>
<p>
  These islands already in the database have similar names to <?= htmlspecialchars($island_name) ?>. If you play on the same console with someone else, they may have already registered their (also your) island. If one of the islands below is indeed your island, ask one of its registered members to send this command in chat:<br>
  <code>/accept_link_island@<?= TELEGRAM_USERNAME ?> <?= $player_id ?> <?= hash_hmac('sha256', 'accept_link_island:'.$player_id, SERVER_HMAC_SECRET) ?></code><br>
  When completed, refresh this page. If you are automatically redirected to the "Update Island Information" page, you have succeeded.
</p>
<ul>
  <?php foreach ($search_results as $search_result) { ?>
  <li>
    <b><?= htmlspecialchars($search_result['name']) ?></b><br>
    Members: <?= htmlspecialchars(implode(', ', $search_result['members'])) ?>
  </li>
  <?php } ?>
</ul>
<p>If non of the above islands are yours, you can create a new one with the name <?= htmlspecialchars($island_name) ?>.</p>
<p><a class="btn btn-secondary" href="<?= htmlspecialchars(WEB_ROOT_URL.'/update-island-info.php?'.http_build_query(array_merge([
  'island_name' => $_POST['island_name'],
], $telegram_login_parameters))) ?>">Create New Island</a></p>
<?php } else { ?>
<form method="post">
  <div class="form-group">
    <label for="form-island-name">Island Name</label>
    <input type="text" class="form-control" id="form-island-name" name="island_name" aria-describedby="form-island-name-help">
    <small id="form-island-name-help" class="form-text">The island name created on the first day in game.</small>
  </div>
  <button type="submit" class="btn btn-success">Next</button>
</form>
<?php } ?>
<?php
webpage_tail();
