<?php

require_once 'config.php';
require_once 'lib/shared_task.php';
require_once 'lib/key_times.php';


if (php_sapi_name() == 'cli') {
  // Close all islands that has passed planned close time
  foreach (run_sql('SELECT id FROM islands WHERE planned_close_time AND planned_close_time <= :now', ['now' => time()]) as $due_island) {
    close_island($due_island['id'], 'it has passed its planned close time');
  }

  // Close all islands that had been opened before 5 am
  foreach (run_sql('SELECT id, open_time, timezone FROM islands WHERE open_time') as $due_island) {
    if ($due_island['open_time'] < key_time('new_day', 'previous', $due_island['timezone'])->getTimestamp()) {
      close_island($due_island['id'], 'it was opened before 5:00 today');
    }
  }

  // Clear no longer relevant island information
  foreach (run_sql('SELECT id, timezone, last_info_update_time FROM islands') as $due_island) {
    if ($due_island['last_info_update_time'] < key_time('shop_close', 'previous', $due_island['timezone'])->getTimestamp()) {
      run_sql('UPDATE islands SET turnip_sell_price = NULL, hot_item_1 = NULL, hot_item_2 = NULL WHERE id = :island_id', ['island_id' => $due_island['id']]);
    }
    if ($due_island['last_info_update_time'] < key_time('daisy_mae_leave', 'previous', $due_island['timezone'])->getTimestamp()) {
      run_sql('UPDATE islands SET turnip_buy_price = NULL WHERE id = :island_id', ['island_id' => $due_island['id']]);
    }
    if ($due_island['last_info_update_time'] < key_time('new_day', 'previous', $due_island['timezone'])->getTimestamp()) {
      run_sql('UPDATE islands SET meteor_shower = FALSE, comment = NULL WHERE id = :island_id', ['island_id' => $due_island['id']]);
    }
  }
}
