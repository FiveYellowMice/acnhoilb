.PHONY: init

init: .commands_set data/db.db

.commands_set: bot_commands.yaml
	curl -X POST \
		-H 'Content-Type: application/json; charset=utf-8' \
		-d "{\"commands\": $$(php -r 'echo json_encode(yaml_parse_file("bot_commands.yaml"));')}" \
		https://api.telegram.org/bot$$(php -r 'require "config.php"; echo TELEGRAM_TOKEN;')/setMyCommands
	touch .commands_set

data/db.db: init.sql
	mkdir -p data
	rm -f data/db.db
	sqlite3 data/db.db < init.sql
	-[ -e test_data.sql ] && sqlite3 data/db.db < test_data.sql
