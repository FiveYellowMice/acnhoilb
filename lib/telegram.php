<?php

function send_api_request($method, $params) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_URL, 'https://api.telegram.org/bot'.TELEGRAM_TOKEN.'/'.$method);
  curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json; charset=utf-8' ]);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
  if (array_key_exists('http_proxy', $_SERVER)) {
    curl_setopt($ch, CURLOPT_PROXY, $_SERVER['http_proxy']);
  }
  $query = json_decode(curl_exec($ch), true);
  curl_close($ch);

  if (!$query) {
    return false;
  }

  if (!array_key_exists('result', $query)) {
    if (array_key_exists('description', $query)) {
      trigger_error('Telegram API error: '.$query['description']);
    }
    return false;
  }

  $result = $query['result'];
  return $result;
}

function group_only_operation($message) {
  if (!in_array($message['chat']['type'], ['group', 'supergroup'])) {
    send_api_request('sendMessage', [
      'chat_id' => $message['chat']['id'],
      'text' => 'This operation can only be done in groups.',
    ]);
    die();
  }
}

function registered_only_operation($message, $island_id) {
  if (!$island_id) {
    send_api_request('sendMessage', [
      'chat_id' => $message['chat']['id'],
      'text' => "You haven't registered or don't have an island yet. You can register with the <code>/register@".TELEGRAM_USERNAME."</code> command.",
      'disable_web_page_preview' => true,
      'parse_mode' => 'HTML',
    ]);
    die();
  }
}

// Convert island info array into a string suitable for sending in messages with parse_mode HTML
// Required fields: timezone, planned_close_time, turnip_sell_price, turnip_buy_price, hot_item_1. hot_item_2, meteor_shower, comment
function stringify_island_information($island_info) {
  $lines = [];

  $local_time = new DateTime('now', new DateTimeZone($island_info['timezone']));
  $lines[] = 'Local time: '.$local_time->format('D. Y-m-d H:i T');

  if ($island_info['planned_close_time']) {
    $closing_in = $local_time->diff(new DateTime('@'.$island_info['planned_close_time']));
    $closing_in_h = intval($closing_in->format('%a')) * 24 + $closing_in->h;
    $lines[] = 'Closing in: '.
      ($closing_in->invert ?
        '0 minutes'
        :
        ($closing_in_h ?
          $closing_in_h.' hour'.($closing_in_h == 1 ? '' : 's').' '
          :
          ''
        )
        .$closing_in->i.' minute'.($closing_in->i == 1 ? '' : 's')
      );
  }

  if ($island_info['turnip_buy_price']){
    $lines[] = 'Turnip Buy Price: '.$island_info['turnip_buy_price'];
  } elseif ($island_info['turnip_sell_price']) {
    $lines[] = 'Turnip Price: '.$island_info['turnip_sell_price'];
  }

  if ($island_info['hot_item_1']) {
    if ($island_info['hot_item_2']) {
      $lines[] = 'Hot Items: '.$island_info['hot_item_1'].' & '.$island_info['hot_item_2'];
    } else {
      $lines[] = 'Hot Item: '.$island_info['hot_item_1'];
    }
  }

  if ($island_info['meteor_shower']) {
    if (intval($local_time->format('G')) >= 19 || intval($local_time->format('G')) < 5) {
      $lines[] = 'Meteor Shower: Now';
    } else {
      $lines[] = 'Meteor Shower: Tonight';
    }
  }

  if ($island_info['comment']) {
    $lines[] = 'Comment: '.htmlspecialchars(str_replace("\n", ' ', $island_info['comment']));
  }

  return implode("\n", $lines);
}
