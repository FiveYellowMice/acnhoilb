<?php

$db = new PDO('sqlite:data/db.db', null, null, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_STRINGIFY_FETCHES => false, PDO::ATTR_EMULATE_PREPARES => false]);

function run_sql($sql, $params = []) {
  global $db;
  $stmt = $db->prepare($sql);
  foreach ($params as $param_name => $param_value) {
    if (is_int($param_value)) {
      $param_type = PDO::PARAM_INT;
    } elseif (is_bool($param_value)) {
      $param_type = PDO::PARAM_BOOL;
    } else {
      $param_type = PDO::PARAM_STR;
    }
    if (preg_match('/:'.$param_name.'\b/', $sql)) {
      $stmt->bindValue(':'.$param_name, $param_value, $param_type);
    }
  }
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
