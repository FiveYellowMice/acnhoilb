<?php

$telegram_login_parameters = array_filter([
  'auth_date' => @$_GET['auth_date'],
  'first_name' => @$_GET['first_name'],
  'id' => @$_GET['id'],
  'last_name' => @$_GET['last_name'],
  'photo_url' => @$_GET['photo_url'],
  'username' => @$_GET['username'],
  'hash' => @$_GET['hash'],
], function($v) { return $v; });

function verify_telegram_login() {
  global $telegram_login_parameters;
  $data_check_string = '';
  foreach ($telegram_login_parameters as $k => $v) {
    if ($k == 'hash') continue;
    $data_check_string .= $k.'='.$v."\n";
  }
  $data_check_string = rtrim($data_check_string);

  $key = hash('sha256', TELEGRAM_TOKEN, true);
  $hash = hash_hmac('sha256', $data_check_string, $key, false);
  if (hash_equals($_GET['hash'], $hash)) {
    return intval($_GET['id']);
  } else {
    http_response_code(403);
    echo "Unauthorized.\n";
    die();
  }
}

function verify_player_registered($telegram_user_id) {
  $player_id = run_sql('SELECT id FROM players WHERE telegram_user_id = :user_id', ['user_id' => $telegram_user_id])[0]['id'] ?? null;

  if ($player_id) {
    return $player_id;
  } else {
    http_response_code(403);
    echo "You haven't registered yet, please register with the <code>/register@".TELEGRAM_USERNAME."</code> command.\n";
    die();
  }
}

function webpage_head($page_title) {
?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= htmlspecialchars($page_title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/@fortawesome/fontawesome-free@5.13.0/css/all.min.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" integrity="sha384-Re/I8sla3AIFEoz6DrgkHWaLDSWKXaLOYm5yn3Gwmf/QhWS3/ajG7O2qr1zBmF3d" crossorigin="anonymous">
    <style>
      code {
        word-break: break-all;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1 class="my-4"><?= htmlspecialchars($page_title) ?></h1>
<?php
}

function webpage_tail() {
?>
    </div>
    <footer class="container mt-5 pb-5 text-muted">
      ACNH Open Island List<br>
      Made with <img src="https://ac-turnip.com/favicon/favicon-32x32.png" width="16" height="16" alt="turnip"> by FiveYellowMice.
    </footer>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/moment@2.24.0/moment.js" integrity="sha384-Z9rov0QZQoXJqe/hrdm93ID90tM8OLFUXcIsBnQosgHNQ6wBRqdH+DoIrUkBbxH5" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js" integrity="sha384-97pmbvZb0qdIXSwrjd4JuG6+4TJhp4eL0totTka0FF1Bf2E42dPiSXsqq09yQxjD" crossorigin="anonymous"></script>
  </body>
</html>
<?php
}
