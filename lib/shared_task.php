<?php

require_once 'db.php';
require_once 'telegram.php';


function open_island($island_id, $player_id, $planned_close_time, $privacy, $dodo_code) {
  run_sql('UPDATE islands SET opened_by = :opened_by, open_time = :open_time, planned_close_time = :planned_close_time, privacy = :privacy, dodo_code = :dodo_code WHERE id = :id', [
    'opened_by' => $player_id,
    'open_time' => time(),
    'planned_close_time' => $planned_close_time ? $planned_close_time->getTimestamp() : null,
    'privacy' => $privacy,
    'dodo_code' => $dodo_code,
    'id' => $island_id,
  ]);

  $player_name = run_sql('SELECT name FROM players WHERE id = :player_id', ['player_id' => $player_id])[0]['name'];
  $island_info = run_sql('SELECT name, timezone, planned_close_time, privacy, dodo_code, turnip_sell_price, turnip_buy_price, hot_item_1, hot_item_2, meteor_shower, comment FROM islands WHERE id = :island_id', ['island_id' => $island_id])[0];

  // Broadcast new open island to all groups the player is part of
  foreach (run_sql('SELECT telegram_chat_id, telegram_broadcast_enabled FROM group_players INNER JOIN "groups" ON "groups".id = "group" WHERE player = :player_id', ['player_id' => $player_id]) as $group) {
    if ($group['telegram_broadcast_enabled']) {
      send_api_request('sendMessage', [
        'chat_id' => $group['telegram_chat_id'],
        'text' => "<b>New Open Island</b>\n".htmlspecialchars($player_name).' has opened their island '.htmlspecialchars($island_info['name']).' to '.['All' => 'everyone', 'Friends' => 'all their friends', 'Best Friends' => 'their best friends'][$island_info['privacy']].($island_info['dodo_code'] ? ' with Dodo Code [<code>'.$island_info['dodo_code'].'</code>]' : '').".\n".stringify_island_information($island_info),
        'disable_web_page_preview' => true,
        'parse_mode' => 'HTML',
      ]);
    }
  }
}

function close_island($island_id, $reason = null) {
  $info = run_sql('SELECT islands.name as island_name, islands.opened_by as player_id, players.name as player_name FROM islands INNER JOIN players ON players.id = islands.opened_by WHERE islands.id = :island_id', ['island_id' => $island_id])[0];

  run_sql('UPDATE islands SET opened_by = NULL, open_time = NULL, planned_close_time = NULL, privacy = NULL, dodo_code = NULL WHERE id = :island_id', ['island_id' => $island_id]);

  // Broadcast closed island to all groups the island opener is part of
  foreach (run_sql('SELECT telegram_chat_id, telegram_broadcast_enabled FROM group_players INNER JOIN "groups" ON "groups".id = "group" WHERE player = :player_id', ['player_id' => $info['player_id']]) as $group) {
    if ($group['telegram_broadcast_enabled']) {
      send_api_request('sendMessage', [
        'chat_id' => $group['telegram_chat_id'],
        'text' => "<b>Island Closed</b>\n".($reason ?
          htmlspecialchars($info['player_name'])."'s island ".htmlspecialchars($info['island_name']).' has been closed because '.$reason.'.'
          : htmlspecialchars($info['player_name']).' has closed their island '.htmlspecialchars($info['island_name']).'.'
        ),
        'disable_web_page_preview' => true,
        'parse_mode' => 'HTML',
      ]);
    }
  }
}
