<?php


function key_time($name, $relative, $timezone) {
  $key_times = [
    'new_day' => '05:00',
    'shop_close' => '22:00',
    'daisy_mae_leave' => '12:00',
  ];

  if (!array_key_exists($name, $key_times)) return null;
  $time = DateTime::createFromFormat('H:i', $key_times[$name], new DateTimeZone($timezone));

  $now = new DateTime('now');
  if ($relative == 'previous' && $now < $time) {
    $time->sub(new DateInterval('P1D'));
  } elseif ($relative == 'next' && $now >= $time) {
    $time->add(new DateInterval('P1D'));
  }

  return $time;
}
